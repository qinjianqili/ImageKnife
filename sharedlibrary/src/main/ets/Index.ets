/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


// ImageKnife 对外暴露的接口由library提供

/**
 * cache
 */

export { FileUtils } from '@ohos/imageknife'
export { Base64 } from '@ohos/imageknife'
export { LruCache } from '@ohos/imageknife'
export { DiskStrategy } from '@ohos/imageknife'
export { ALL } from '@ohos/imageknife'
export { AUTOMATIC } from '@ohos/imageknife'
export { DATA } from '@ohos/imageknife'
export { NONE } from '@ohos/imageknife'
export { RESOURCE } from '@ohos/imageknife'
export { EngineKeyInterface } from '@ohos/imageknife'
export { EngineKeyFactories } from '@ohos/imageknife'

/**
 * compress
 */
export { CompressBuilder } from '@ohos/imageknife'
export { OnCompressListener } from '@ohos/imageknife'
export { OnRenameListener } from '@ohos/imageknife'
export { CompressDataListener } from '@ohos/imageknife'
export { CompressionPredicate } from '@ohos/imageknife'
export { CompressAdapter } from '@ohos/imageknife'
export { CompressProvider } from '@ohos/imageknife'
export { DataStringPathProvider } from '@ohos/imageknife'
export { RecourseProvider } from '@ohos/imageknife'

/**
 * crop
 */

export { CropImage } from '@ohos/imageknife'
export { CropOptions } from '@ohos/imageknife'
export { PixelMapCrop,Options } from '@ohos/imageknife'
export { CropCallback } from '@ohos/imageknife'

/**
 * transform
 */
export { BaseTransform } from '@ohos/imageknife'
export { BlurTransformation } from '@ohos/imageknife'
export { BrightnessFilterTransformation } from '@ohos/imageknife'
export { ContrastFilterTransformation } from '@ohos/imageknife'
export { CropCircleTransformation } from '@ohos/imageknife'
export { CropCircleWithBorderTransformation,rgbColor } from '@ohos/imageknife'
export { CropSquareTransformation } from '@ohos/imageknife'
export { CropTransformation,CropType } from '@ohos/imageknife'
export { GrayscaleTransformation } from '@ohos/imageknife'
export { InvertFilterTransformation } from '@ohos/imageknife'
export { PixelationFilterTransformation } from '@ohos/imageknife'
export { RotateImageTransformation } from '@ohos/imageknife'
export { RoundedCornersTransformation,RoundCorner } from '@ohos/imageknife'
export { SepiaFilterTransformation } from '@ohos/imageknife'
export { SketchFilterTransformation } from '@ohos/imageknife'
export { MaskTransformation } from '@ohos/imageknife'
export { SwirlFilterTransformation } from '@ohos/imageknife'
export { KuwaharaFilterTransform } from '@ohos/imageknife'
export { ToonFilterTransform } from '@ohos/imageknife'
export { VignetteFilterTransform } from '@ohos/imageknife'
export { TransformUtils } from '@ohos/imageknife'
export { TransformType } from '@ohos/imageknife'
export { CenterCrop } from '@ohos/imageknife'
export { CenterInside } from '@ohos/imageknife'
export { FitCenter } from '@ohos/imageknife'

/**
 * pngj
 */
export { Pngj } from '@ohos/imageknife'
export {handler} from '@ohos/imageknife'
export { UPNG } from '@ohos/imageknife'



/**
 * ImageKnife
 */
export { ImageKnife } from '@ohos/imageknife'
export { ImageKnifeGlobal } from '@ohos/imageknife'
export {RequestOption,Size} from '@ohos/imageknife'
export {ObjectKey} from '@ohos/imageknife'
export { ImageKnifeComponent, ScaleType, ScaleTypeHelper, AntiAliasing} from '@ohos/imageknife'
export { ImageKnifeDrawFactory } from '@ohos/imageknife'
export {ImageKnifeOption,CropCircleWithBorder,Crop,GifOptions,TransformOptions} from '@ohos/imageknife'
export { ImageKnifeData } from '@ohos/imageknife'
export {IAllCacheInfoCallback,AllCacheInfo,ResourceCacheInfo,MemoryCacheInfo,DataCacheInfo}  from '@ohos/imageknife'
export {IParseImage} from '@ohos/imageknife'
export {IDataFetch} from '@ohos/imageknife'
export {ICache} from '@ohos/imageknife'
export { FileTypeUtil } from '@ohos/imageknife'
export { ParseImageUtil } from '@ohos/imageknife'

/**
 * svg parse
 */
export { SVGParseImpl } from '@ohos/imageknife'

/**
 * gif parse
 */
export { GIFParseImpl } from '@ohos/imageknife'
export { GIFFrame } from '@ohos/imageknife'


// 自定义组件新增
// 自定义组件绘制生命周期
export { IDrawLifeCycle } from '@ohos/imageknife'

// 日志管理
export { LogUtil } from '@ohos/imageknife'

// 额外开放初始化ImageKnife的方法
export {InitImageKnife} from '../ets/pages/InitImageKnife'

export {GetRes1} from '../ets/pages/GetRes1'